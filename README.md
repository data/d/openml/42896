# OpenML dataset: behavior-urban-traffic

https://www.openml.org/d/42896

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The database was created with records of behavior of the urban traffic of the city of Sao Paulo in Brazil from December 14, 2009 to December 18, 2009 (From Monday to Friday). Registered from 7:00 to 20:00 every 30 minutes. The data set Behavior of the urban traffic of the city of Sao Paulo in Brazil was used in academic research at the Universidade Nove de Julho - Postgraduate Program in Informatics and Knowledge Management.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42896) of an [OpenML dataset](https://www.openml.org/d/42896). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42896/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42896/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42896/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

